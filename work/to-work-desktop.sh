#!/bin/bash

xrandr --output DP1 --mode 2560x1440 --right-of eDP1 && ~/.xprofile
xinput --set-prop 'Logitech M705' 'Device Accel Constant Deceleration' 0.82
