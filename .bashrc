# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User defined aliases/functions

if [ -r ~/.profile ]; then source ~/.profile; fi

# Bash Scripts
for script in ~/.bash_scripts/*; do
  source $script;
done

# disable Ctrl-S freezing VIM >_>
stty -ixon

alias ls="ls --color"
alias poweroff="systemctl poweroff"

#Elinks aliases
alias google='/usr/bin/env elinks https://google.com'

# Work Aliases
alias supporttools='ssh smiller@supporttools.instructure.com'

alias flux='xflux -z 84111 -k 2400'

# Git Aliases
alias gits='git status'
alias gerrit='git push origin HEAD:refs/for/master'

# God Tier Aliases
alias c='xclip -selection clipboard'
alias v='xclip -selection clipboard -o'

# Program Aliases
alias vpn='sudo openconnect vpn.instructure.com'

# Setup Prompt
source ~/.git-completion.sh
source ~/.git-prompt.sh
source ~/.bash_colors

PROMPT_COMMAND='__git_ps1 "$Cyan\A [\#:\j] ${BIGreen}\w$Color_Off" " $Green\\\$ $Color_Off"'
PROMPT_DIRTRIM=2
#trap 'echo -en "\e[0m"' DEBUG

GIT_PS1_SHOWDIRTYSTATE=1
GIT_PS1_SHOWSTASHSTATE=1
GIT_PS1_SHOWUPSTREAM="auto"
GIT_PS1_SHOWCOLORHINTS=1

# direnv
eval "$(direnv hook bash)"

PGVM_ENV_PATH="$HOME/.pgvm/pgvm_env"
if [ -r $PGVM_ENV_PATH ]; then source $PGVM_ENV_PATH; fi
