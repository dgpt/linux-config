# Load .bashrc and other dependencies
if [ -r "$HOME/.bash_colors" ]; then . "$HOME/.bash_colors"; else echo "Bash color file not found. Please check location in $HOME/.bash_profile"; fi

## Env Variables
export EDITOR=vim
export BASH=/bin/bash
export SHELL=$BASH

export ANDROID_HOME=/usr/local/programs/android-studio/sdk
export ANDROID_STUDIO=/usr/local/programs/android-studio/bin

export CHROME_BIN=/usr/bin/chromium-browser
export FIREFOX_BIN=/opt/firefox/firefox-bin
export FIREFOX=/opt/firefox

export PGHOST=localhost
export POSTGRESQL_BIN=/usr/lib/postgresql/9.1/bin

export DROPBOX="$HOME/.dropbox-dist/"

PATH=$PATH:$POSTGRESQL_BIN:$GEM_BIN:$ANDROID_STUDIO:$FIREFOX_BIN:$CHROME_BIN:$DROPBOX:'~/bin'

